#README#
Tarea para DIW08.
Detalles de la tarea de esta unidad.
Enunciado.

La tarea que debes de realizar consiste en reformar un viejo código de un formulario en un código que sea estándar según las recomendaciones del W3C para el XHTML 1.0, que valide tanto el código XHTML como las hojas de estilo con las herramientas de validación de la W3C, que sea semántico y accesible según las herramientas que vimos para la verificación de la accesibilidad, que sea usable y dotado de interactividad independiente del dispositivo empleando CSS.
Para realizar la tarea deberás descargar el código inicial indicado en los recursos y hacer los cambios que consideres oportunos en el código para generar un código XHTML semántico y válido, y un archivo con los estilos enlazado al documento anterior y empaquetar ambos en un archivo comprimido.

Criterios de puntuación. Total 10 puntos.
Código XHTML válido: 1 punto.
CSS válido: 1 punto.
Código semántico: 1 punto.
Código accesible: 2 puntos.
Código usable: 2 puntos.
Interactivo: 3 puntos.

Recursos necesarios para realizar la Tarea.
Desde el siguiente enlace podrás descargar el código inicial con el que deberás realizar la tarea.
Enlace al código inicial del que hay que partir para realizar la tarea
Podrás validar tu código XHTML desde el siguiente enlace:
Enlace a la página del validador del (X)HTML del W3C.
Podrás validar tu código CSS desde el siguiente enlace:
Enlace a la página del validador de CSS del W3C.
Podrás verificar la accesibilidad desde el siguiente enlace:
Enlace a la página de la herramienta WAVE para la verificación de la accesibilidad.
Enlace a la página de la herramienta ACheker para la verificación de la accesibilidad.
Consejos y recomendaciones.

El código inicial, si lo ejecutas en el navegador, verás que se muestra correctamente, tiene algunos errores de sintaxis, olvidos y no es semántico ya que utiliza las tablas como forma de maquetar el formulario en lugar del modelo de cajas que viste en la unidad de Hojas de estilo. Las tablas sólo se deben emplear para colocar datos tabulares y un formulario no lo es.
Para poder llevar a buen término la tarea te aconsejo que vayas creando versiones del documento a medida que vayas haciendo cambios porque así siempre podrás recurrir, en caso de error, a alguno de ellos.
Es conveniente que sigas los siguientes pasos:

•	Abrir en tu navegador habitual los validadores correspondientes al código XHTML estricto y CSS3 en la opción de validar el fichero. Recuerda que la validación se podía hacer mediante URL, fichero o código escrito directamente. Utiliza la opción de validar fichero, es más cómoda y, de esta forma, cada vez que realices modificaciones importantes en tu fichero podrás validar la nueva versión.

•	Ve comparando el aspecto en el navegador de tus nuevas versiones con respecto a la versión original para tratar de conseguir un parecido razonable e, incluso, mejorable, ya que la fuente de todo el formulario debería ser de color púrpura y no sólo las etiquetas. La razón de que el original no mostrase este color es que sin el uso de las hojas de estilos esto no se podía hacer.
•	Comprueba que al menos se ve correctamente en el Internet Explorer y el Firefox ya que son los navegadores más usados en el 2010 y el 2011 tanto en el mundo entero como en Europa, aunque no estaría de más que lo comprobaras también en Google Chrome ya que está ganando puestos de año en año y es el segundo navegador utilizado en América del sur, por encima del Firefox. Ten en cuenta que en este continente hay mucho mercado potencial de habla hispana, y si desarrollas interfaces web para sitios comerciales no está de más ampliar el abanico de posibilidades. Cuantos más navegadores pruebes siempre será mejor.
En el enlace siguiente puedes comprobar cuáles son los navegadores más usados en el mundo, en América del norte, en América del sur y en Europa.

Enlace a la página que contiene el artículo de la comparativa de los navegadores más usados en el año 2011.

•	Comprueba tu versión con diferentes herramientas de verificación de la accesibilidad, elimina todos los errores que te den y fíjate en las advertencias ya que éstas hacen hincapié algunas veces sobre la semántica de los textos. Los textos deben ser aclaratorios de la funcionalidad del sitio que estás diseñando para que las personas con discapacidad visual no se pierdan durante la navegación.

•	Una vez que tienes un código accesible éste tiene que seguir siendo válido sintácticamente. La validación del código es un proceso que debes realizar de vez en cuando ya que siempre te puedes equivocar al teclear.

•	Haz el formulario más usable a la vez que accesible, que se pueda usar con el ratón y el teclado, y con ayudas contextuales de lo que es cada elemento de forma que el usuario no tenga duda sobre su significado.

•	Añade la interactividad mediante CSS al final de todo.

•	Aunque podrías tener las hojas de estilo incrustadas, es conveniente separar el contenido de la presentación por lo que ten ambas cosas en archivos separados y vincula la hoja de estilo a tu documento con la orden correspondiente que ya has estudiado, así no habrá problemas con los navegadores que no reconocen la etiqueta <style>.

•	Recuerda que la página debe estar preparada para ratón y teclado permitiendo a los usuarios que por alguna discapacidad no pueden usar el ratón puedan utilizar la interfaz con el teclado. Utiliza adecuadamente la ordenación de los elementos del formulario y pon una tecla de acceso directo a cada elemento que se considera obligatorio: Nombre, primer apellido, segundo apellido, sexo (Hombre o Mujer), edad (en cada grupo de edad) y país de nacimiento.

•	Marca los elementos obligatorios para que el usuario los distinga mediante un asterisco a la izquierda de su etiqueta, que deberá ir en otro color mediante la aplicación de un estilo propio, e indica el significado de los asteriscos al inicio y al final del formulario mediante texto con ese mismo color.

•	Los campos obligatorios deben estar señalizados con el marcado del lenguaje.

En el siguiente enlace puedes obtener más información de cómo hacer accesible un formulario.

Enlace a la página de INTECO donde se aporta un Listado de buenas prácticas sobre Formularios.

